# Parse commands from flags

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --sumo_access_id)
    SUMO_ACCESS_ID="$2"
    shift # past argument
    shift # past value
    ;;
    --sumo_secret_key)
    SUMO_SECRET_KEY="$2"
    shift # past argument
    shift # past value
    ;;
    --collector_path)
    COLLECTOR_PATH="$2"
    shift # past argument
    shift # past value
    ;;
    --collector_name)
    COLLECTOR_NAME="$2"
    shift # past argument
    shift # past value
    ;;
    --collector_desc)
    COLLECTOR_DESCRIPTION="$2"
    shift # past argument
    shift # past value
    ;;
    --sumo_region)
    SUMO_REGION="$2"
    shift # past argument
    shift # past value
    ;;
    --help)
    HELP=YES
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ "$HELP" == "YES" ]
then
    echo "./create_sumo_collector.sh <args>"
    echo "  --collector_path  - the source_category to use for the collector (required)"
    echo "  --sumo_secret_key - the secret key to use for sumologic's API (required)"    
    echo "  --sumo_access_id  - the access key id to use for sumologic's API (required)"
    echo "  --collector_name  - the name of the collector (optional, defaults to collector_path)"
    echo "  --collector_desc  - the description of the collector (optional, defaults to collector_path)"
    echo "  --sumo_region     - the 'region' of sumologic to hit with your command (I.e., us1 vs us2) (optional, defaults to us2)"
    echo "  --help            - prints this message, then exits."
	exit 1
fi

# Check that all three required variables are passed in 
if [ -z "$SUMO_ACCESS_ID" ] || [ -z "$SUMO_SECRET_KEY" ] || [ -z "$COLLECTOR_PATH" ] 
then 
    echo "variables missing. Use './create_sumo_collector.sh --help' for possible inputs"
    # Exit with an error code, we need all 4 variables
    exit -1
fi

# populate defaults on remaining variables if they're empty
if [ -z "$COLLECTOR_NAME" ]
then
    COLLECTOR_NAME="$COLLECTOR_PATH"
fi
if [ -z "$COLLECTOR_DESCRIPTION" ]
then
    COLLECTOR_DESCRIPTION="$COLLECTOR_PATH"
fi
if [ -z "$SUMO_REGION" ]
then
    SUMO_REGION="us2"
fi

## From here on out we will assume a 1:1 mapping between collectors and sources. We could potentially add another "collector_id" input variable, and 
##      add the source to an existing collector if said variable is present. For now, collectors themselves are free, so there is no harm in 1:1.

# use curl to call the API
RESPONSE=`curl -s -u "$SUMO_ACCESS_ID:$SUMO_SECRET_KEY" -X POST -H "Content-Type: application/json" -d "{\"collector\":{\"collectorType\":\"Hosted\",\"name\":\"$COLLECTOR_NAME\",\"description\":\"$COLLECTOR_DESCRIPTION\",\"category\":\"$COLLECTOR_PATH\"}}" "https://api.$SUMO_REGION.sumologic.com/api/v1/collectors"`
echo "$RESPONSE" >> "collector_information_$COLLECTOR_NAME.json"

COLLECTOR_ID=`echo "$RESPONSE" | grep -Po '\"id\":\K([0-9]*)'`

if [ -z "$COLLECTOR_ID" ]
then
    echo "No collector ID parsed. Collector creation likely failed (duplicate name?)"
    exit -2
else    
    #user curl to create the "source" within the collector - use the Collector ID from the previous command to attach them.
    SOURCE_RESPONSE=`curl -s -u "$SUMO_ACCESS_ID:$SUMO_SECRET_KEY" -X POST -H "Content-Type: application/json" -d "{\"source\":{\"sourceType\":\"HTTP\",\"name\":\"$COLLECTOR_NAME\",\"description\":\"$COLLECTOR_DESCRIPTION\",\"category\":\"$COLLECTOR_PATH\", \"messagePerRequest\":false}}" "https://api.$SUMO_REGION.sumologic.com/api/v1/collectors/$COLLECTOR_ID/sources/"`
    echo "$SOURCE_RESPONSE" >> "source_information_$COLLECTOR_NAME.json"
    
    #This is sort of a hilariously complex regex. Basically, it will grab everything from the URL parameter of the JSON, and will return it up to the trailing comma.
    #     This should represent the URL that terraform will want for the sumologic collector.
    SOURCE_URL=`echo "$SOURCE_RESPONSE" | grep -Po '\"url\":\"\K[a-zA-Z:/.0-9_=\-]*(?!,)'`

	echo "Successfully Created Collector: $COLLECTOR_PATH"
	echo "URL: $SOURCE_URL"
	exit 1
fi  

