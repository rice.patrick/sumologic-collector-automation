# SumoLogic Collector and Source bash script

A very simple bash script for automatically creating a hosted controller (and one HTTP Source) within sumologic. The script doesn't allow significant customization at the current time.

## Configuration

The script accepts a set of flags to configure the API call:

`--collector_path` - the "Source Category" that will be used for the source under the collector. Will also be used for the name and description if those are not provided. This value is required.

`--sumo_access_id` - the "Access Key" from SumoLogic. Required for the use of the API. See [https://help.sumologic.com/Manage/Security/Access-Keys](SumoLogic Access Key) documentation.

`--sumo_secret_key`- the "Secret Key" from SumoLogic. See above documentation.

`--collector_name` - the "Collector Name" that will be used for the collector and source in SumoLogic. Will default to the collector_path if not specified.

`--collector_desc` - the "Collector Description" that will be used for the collector and source in SumoLogic. Will default to the collector_path if not specified.

`--sumo_region`    - the "Region" of sumologic that your deployment exists within. Is the second level sub-domain in all sumologic deployments. Defaults to 'us2'.

`--help`           - prints a list of options.

### Example Call

Because of the way the script parses arguments (a very lazy implementation) an equals sign after the flag will cause it to not parse the argument correctly. Make sure you separate the flag from the value with a space instead.

`./create_sumo_collector.sh --collector_path "/example/collector/" --sumo_access_id 1234abcd --sumo_secret_key 11111222233334444aaaabbbbccccdddd`
